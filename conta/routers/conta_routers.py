from decimal import Decimal
from fastapi import APIRouter
from pydantic import BaseModel
from typing import List 

router = APIRouter(prefix='/contas')

class ContaResponse( BaseModel ):
    id: int 
    descricao: str
    valor: Decimal
    tipo: str 

class ContaRequest( BaseModel ):
    descricao: str
    valor: Decimal
    tipo: str 


@router.get('', response_model=List[ContaResponse] , status_code=200)
async def listar_contas(): 
    return[
        ContaResponse(id=1,descricao="errr",valor=3.3,tipo='pagar'),
    ]


@router.post('', response_model=ContaResponse , status_code=201 )
async def criar_conta(conta: ContaRequest):

    return ContaResponse(
        id=4,
        descricao=conta.descricao,
        valor=conta.valor,
        tipo=conta.tipo,
    )