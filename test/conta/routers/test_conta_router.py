from fastapi.testclient import TestClient
from main import app 

client = TestClient(app)


def test_listar_contas():
    response = client.get('/contas')
    assert response.status_code == 200 
    assert response.json() == [
        {'id': 1, 'descricao': 'errr', 'valor': '3.3', 'tipo': 'pagar'},
    ]


def teste_cria_conta():

    conta = {
        "descricao":"test case",
        "valor":33,
        "tipo": "teste case",
    }
    
    conta_copy = conta.copy()
    conta_copy['id'] = 4
    
    response = client.post('/contas' , json=conta)

    assert response.status_code == 201

    response_json = response.json()
    response_json['valor'] = int(response_json['valor'])
    
    assert response_json == conta_copy
