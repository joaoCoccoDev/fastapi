from fastapi import FastAPI 
from conta.routers import conta_routers


app = FastAPI()

@app.get("/")
def root():
    return {"message": "Hello World"}

app.include_router(conta_routers.router)

